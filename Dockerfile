FROM openjdk:11.0.12-jre-slim

COPY target/*.jar /home/sengen/sengen.jar

WORKDIR /home/sengen
EXPOSE 8080
ENTRYPOINT [ "java", "-jar" , "sengen.jar"]