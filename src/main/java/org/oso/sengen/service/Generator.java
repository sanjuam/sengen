package org.oso.sengen.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.oso.sengen.model.Sentence;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.NounRepository;
import org.oso.sengen.repository.VerbRepository;
import org.oso.sengen.repository.WordRepository;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class Generator {

	private final WordRepository wordRepository;
	private final VerbRepository verbRepository;
	private final NounRepository nounRepository;
	private final Random secureRandom;

	private List<Sentence> sentences;

	public List<Sentence> generate() {
		sentences = new ArrayList<>();
		List<Word> subjects = wordRepository.findWordsByNounIsNotNull();
		List<Word> verbs = wordRepository.findWordsByVerbIsNotNull();
		List<Word> dirObjs = wordRepository.findWordsByNounIsNotNull();
		for (int i = 0; i < 5; i++) {
			Word subject = subjects.get(secureRandom.nextInt(subjects.size()));
			Word verb = verbs.get(secureRandom.nextInt(verbs.size()));
			Word dirObj;
			do {
				dirObj = dirObjs.get(secureRandom.nextInt(dirObjs.size()));
			} while (subject.getValue().equalsIgnoreCase(dirObj.getValue()));
			Sentence sentence = new Sentence(subject,verb,dirObj);
			if (sentences.contains(sentence))
				i--;
			else
				sentences.add(sentence);
		}
		return sentences.stream().distinct().collect(Collectors.toList());
	}
}
