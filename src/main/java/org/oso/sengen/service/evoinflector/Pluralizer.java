package org.oso.sengen.service.evoinflector;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.atteo.evo.inflector.English;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.WordRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class Pluralizer {

    private final WordRepository wordRepository;

    public boolean updatePluralForm(List<Word> words) {
        words.forEach(word -> {
            log.info("Updating plural form of: " + word.getValue());
            word.setPlural(English.plural(word.getValue()).toUpperCase());
            wordRepository.save(word);
            log.info(word.getValue() + " has the plural of: " + word.getPlural());
        });
        return true;
    }
}
