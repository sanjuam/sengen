package org.oso.sengen.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Word;
import org.oso.sengen.model.wassocdotnet.WAssocDotNetResponse;
import org.oso.sengen.repository.NounRepository;
import org.oso.sengen.repository.VerbRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class WAssocDotNetService {

    private final ObjectMapper mapper;
    private final VerbRepository verbRepository;
    private final NounRepository nounRepository;
    private static final String BASE_URL = "https://api.wordassociations.net/associations/v1.0/json/search";

    public boolean updateActionsOfANoun(List<Word> words) {
        words.forEach(
                word -> {
                    log.info("Finding associations for: " + word.getValue());
                    try {
                        WAssocDotNetResponse response = mapper.readValue(
                                getVerbAssociations(word).string(),
                                WAssocDotNetResponse.class
                        );
                        response.getResponse().forEach(response1 -> {
                            Noun noun = nounRepository.findNounByWord_Value(word.getValue());
                            log.info("Associating: " + noun.getWord().getValue());
                            updateAssociations(noun, response1);
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    log.info("-------------------------------------");
                }
        );
        return true;
    }

    private ResponseBody getVerbAssociations(Word word) {
        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(BASE_URL).newBuilder();
        urlBuilder.addQueryParameter("apikey", "51fd9721-0906-4a78-a68c-a9b2de5a3b03");
        urlBuilder.addQueryParameter("text", word.getValue());
        urlBuilder.addQueryParameter("lang", "en");
        urlBuilder.addQueryParameter("limit", "300");
        urlBuilder.addQueryParameter("pos","verb");

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.body();
    }

    public boolean updateAssociations(Noun noun, org.oso.sengen.model.wassocdotnet.Response response) {
        response.getItems().forEach(item -> {
            verbRepository.findByWord_Value(item.getItem().toUpperCase())
                    .ifPresent(verb -> {
                        log.info(verb.getWord().getValue()+ "\t" + item.getPos());
                        noun.getVerbs().add(verb);
                    });
        });
        log.info(noun.toString());
        nounRepository.save(noun);
        return true;
    }
}
