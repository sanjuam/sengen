package org.oso.sengen.service.wordsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Verb;
import org.oso.sengen.model.Word;
import org.oso.sengen.model.wordsapi.Result;
import org.oso.sengen.model.wordsapi.WordsAPIResponse;
import org.oso.sengen.repository.WordRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class WordsAPISvc {

    private final ObjectMapper mapper;
    private final WordRepository wordRepository;
    private static final String NOUN = "noun";
    private static final String VERB = "verb";

    public int associateWord2NounsAndVerbs(List<Word> words) {
        int rowsUpdated = 0;

        log.info("Fetching all words..");
        words.forEach(word -> {
            log.info("Working on: " + word.getValue() + "...");
            try {
                WordsAPIResponse wordsAPIResponse = mapper.readValue(
                        getDefinitions(word).string(),
                        WordsAPIResponse.class
                );
                List<Result> results = wordsAPIResponse.getResults();
                long n = results.stream()
                            .filter(result -> result.getPartOfSpeech().equalsIgnoreCase(NOUN))
                            .count();
                long v = results.stream()
                            .filter(result -> result.getPartOfSpeech().equalsIgnoreCase(VERB))
                            .count();
                if(n > 0) {
                    log.info(word.getValue() + " has part of speech that is " + NOUN);
                    Noun noun = new Noun();
                    noun.setWord(word);
                    word.setNoun(noun);
                }
                if(v > 0) {
                    log.info(word.getValue() + " has part of speech that is " + VERB);
                    Verb verb = new Verb();
                    verb.setWord(word);
                    word.setVerb(verb);
                }
                log.info("saved updates for " + word.getValue() + "...OK");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            wordRepository.save(word);
        });


        return rowsUpdated;
    }

    public ResponseBody getDefinitions(Word word) {
        OkHttpClient client = new OkHttpClient();
        String url = "https://wordsapiv1.p.rapidapi.com/words/" + word.getValue();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("x-rapidapi-host", "wordsapiv1.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "639bf64cf8mshddf483d0ac033dcp187ee0jsn11eb8c7c9e04")
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return response.body();
    }

    public boolean associate() {

        return true;
    }

}
