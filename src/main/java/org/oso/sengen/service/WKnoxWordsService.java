package org.oso.sengen.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.oso.sengen.model.WKnoxPluralResponse;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.WordRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WKnoxWordsService {

    private final ObjectMapper mapper;
    private final WordRepository wordRepository;

    public boolean updatePluralForms(List<Word> words) {
        words.forEach(word -> {
            try {
                WKnoxPluralResponse wKnoxPluralResponse =
                        mapper.readValue(getPluralForm(word.getValue()).string(), WKnoxPluralResponse.class);
                word.setPlural(wKnoxPluralResponse.getPlural());
                wordRepository.save(word);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return true;
    }

    private ResponseBody getPluralForm(String word) {
        OkHttpClient client = new OkHttpClient();
        String url = "https://webknox-words.p.rapidapi.com/words/" + word + "/plural";
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("x-rapidapi-host", "webknox-words.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "2f3b57b9abmshf6553a51193c112p182debjsn7822f071f454")
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.body();
    }
}
