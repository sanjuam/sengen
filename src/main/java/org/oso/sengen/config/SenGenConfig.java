package org.oso.sengen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class SenGenConfig {

	@Bean
	public Random getSecureRandom() {
		return new Random();
	}
}
