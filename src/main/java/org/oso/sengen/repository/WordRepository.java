package org.oso.sengen.repository;

import org.oso.sengen.model.Word;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WordRepository extends CrudRepository<Word, Long> {
//	Long countWordByArticleEquals(boolean isArticle);
//	Long countWordByNounEquals(boolean isNoun);
//	Long countWordByVerbEquals(boolean isVerb);
//	Long countWordByLvEquals(boolean isLV);
//	List<Word> findWordsByArticleEquals(boolean isArticle);
//	List<Word> findWordsByNounEquals(boolean isNoun);
//	List<Word> findWordsByVerbAndLvEquals(boolean isVerb, boolean isLV);
//	List<Word> findWordsByNounAndPnEquals(boolean isNoun, boolean isPN);
	List<Word> findWordsByVerbIsNotNull();
	List<Word> findWordsByNoun_Pn(boolean isProperNoun);
	List<Word> findWordsByNounIsNotNull();
	Word findByValueEquals(String value);
}
