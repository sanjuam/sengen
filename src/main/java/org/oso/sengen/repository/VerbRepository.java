package org.oso.sengen.repository;

import org.oso.sengen.model.Verb;
import org.oso.sengen.model.Word;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Repository
public interface VerbRepository extends CrudRepository<Verb, Long> {
//	List<Verb> findVerbsByWord_Verb(boolean isVerb);
    Optional<Verb> findByWord_Value(String word);
    Optional<Verb> findByWord(Word word);
}
