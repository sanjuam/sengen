package org.oso.sengen.repository;

import org.oso.sengen.model.Noun;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NounRepository extends CrudRepository<Noun, Long> {
	List<Noun> findNounsByPnEquals(boolean isProperNoun);
	Noun findNounByWord_Value(String value);
}
