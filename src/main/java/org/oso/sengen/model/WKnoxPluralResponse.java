package org.oso.sengen.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WKnoxPluralResponse {

    private String original;
    private String plural;

}
