package org.oso.sengen.model.wordsapi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class WordsAPIResponse {
    private String word;
    private List<Result> results;
    private Syllables syllables;
    private Pronunciation pronunciation;
    private int frequency;
}
