package org.oso.sengen.model.wordsapi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Syllables {
    private int count;
    private List<String> list;
}
