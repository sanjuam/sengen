package org.oso.sengen.model.wordsapi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Result {
    private String definition;
    private String partOfSpeech;
    private List<String> synonyms;
    private List<String> typeOf;
    private List<String> hasTypes;
    private List<String> instanceOf;
    private List<String> examples;
    private List<String> hasCategories;
    private List<String> hasParts;
    private List<String> partOf;
}
