package org.oso.sengen.model.wordsapi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Pronunciation {
    private String all;
}
