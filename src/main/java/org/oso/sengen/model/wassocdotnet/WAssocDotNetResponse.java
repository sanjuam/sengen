package org.oso.sengen.model.wassocdotnet;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class WAssocDotNetResponse {
    private String version;
    private float code;
    Request request;
    private List<Response> response;
}
