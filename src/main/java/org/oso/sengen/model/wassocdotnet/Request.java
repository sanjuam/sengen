package org.oso.sengen.model.wassocdotnet;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class Request {
    ArrayList< Object > text = new ArrayList < Object > ();
    private String lang;
    private String type;
    private float limit;
    private String pos;
    private String indent;
}
