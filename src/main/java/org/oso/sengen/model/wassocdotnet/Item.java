package org.oso.sengen.model.wassocdotnet;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Item {
    private String item;
    private float weight;
    private String pos;
}
