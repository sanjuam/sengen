package org.oso.sengen.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "NOUN")
@Getter @Setter
public class Noun {

	@Id
	@GeneratedValue(
			strategy = GenerationType.IDENTITY
	)
	private long id;

	@Column(name = "IS_SINGULAR")
	private boolean si;

	@Column(name = "IS_PN")
	private boolean pn;//Proper Noun

	@Column(name = "PLURAL_FORM")
	private String pf; //plural form

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name = "WORD_ID",
			referencedColumnName = "id"
	)
	private Word word;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "NOUNS_VERBS",
			joinColumns =  {
					@JoinColumn(
							name = "NOUN_ID",
							referencedColumnName = "ID",
							nullable = false,
							updatable = false
					)},
			inverseJoinColumns = {
					@JoinColumn(
							name = "VERB_ID",
							referencedColumnName = "ID",
							nullable = false,
							updatable = false
					)}
	)
	private List<Verb> verbs;

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(this.word.getValue());
		stringBuilder.append("  Verbs: [ ");
		for (Verb verb : verbs) {
			stringBuilder.append(verb.getWord().getValue() + "\t");
		}
		stringBuilder.append(" ]");
		return stringBuilder.toString();
	}
}
