package org.oso.sengen.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "WORD")
public class Word {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "IS_ARTICLE")
	private boolean article;

	@Column(name = "IS_OBJ")
	private boolean obj;//object, non living things

	@Column
	private String value;

	@OneToOne(mappedBy = "word", cascade = CascadeType.ALL)
	private Noun noun;

	@OneToOne(mappedBy = "word", cascade = CascadeType.ALL)
	private Verb verb;

	@Column
	private String plural;

}
