package org.oso.sengen.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "VERB")
@Getter @Setter
@NoArgsConstructor
public class Verb {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "PLURAL_FORM")
	private String pf; //plural form

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name = "WORD_ID",
			referencedColumnName = "id"
	)
	private Word word;

	@ManyToMany(
			fetch = FetchType.EAGER,
			mappedBy = "verbs"
	)
	private List<Noun> nouns;
}
