package org.oso.sengen.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Sentence {

	private static String A = "A";
	private static String THE = "THE";
	private static String SPACE = " ";
	private StringBuilder value;

	public Sentence(Word subject, Word verb, Word dirObj) {
		value = new StringBuilder();
		value.append(buildSubject(subject));
		if (subject.getNoun().isSi()) {
			value.append(verb.getValue());
			value.append(verb.getVerb().getPf());
			value.append(SPACE);
		} else {
			value.append(buildVerb(verb));
		}
		value.append(buildDO(dirObj));
	}

	public StringBuilder createSentence() {

		return value;
	}

	public String buildSubject(Word word) {
		StringBuilder builder = new StringBuilder();
		if (!word.getNoun().isPn())
			builder.append(THE + SPACE);
		builder.append(word.getValue() + SPACE);
		return builder.toString();
	}

	public String buildVerb(Word word) {
		return word.getValue() + SPACE;
	}

	public String buildDO(Word word) {
		StringBuilder builder = new StringBuilder();
		if (!word.getNoun().isPn())
			builder.append(THE + SPACE);
		builder.append(word.getValue() + SPACE);
		return builder.toString();
	}
}
