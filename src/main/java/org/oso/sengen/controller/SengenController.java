package org.oso.sengen.controller;

import lombok.RequiredArgsConstructor;
import org.oso.sengen.service.Generator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class SengenController {

	private final Generator generator;

	@GetMapping("/greeting")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}

	@GetMapping("/generate")
	public String generate(Model model) {
		model.addAttribute("sentences", generator.generate());
		return "greeting";
	}


}
