package org.oso.sengen.controller;

import lombok.RequiredArgsConstructor;
import org.oso.sengen.model.Sentence;
import org.oso.sengen.service.Generator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class SengenAPIController {

	private final Generator generator;

	@GetMapping("/generate")
	public List<Sentence> generate() {
		return generator.generate();
	}
}
