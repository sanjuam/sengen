package org.oso.sengen;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.oso.sengen.repository.WordRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class SengenApplication implements ApplicationRunner {

	private final WordRepository wordRepository;

	public static void main(String[] args) {
		SpringApplication.run(SengenApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("All words = " + wordRepository.count());
	}
}
