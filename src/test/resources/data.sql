--WORD Table

    --Person: ID = 1
    INSERT INTO WORD (value, IS_ARTICLE,IS_OBJ) VALUES ('SKY',FALSE ,FALSE);

    --Object
    INSERT INTO WORD (value, IS_ARTICLE,IS_OBJ) VALUES ('TICKET',FALSE ,FALSE);

    --Verb
    INSERT INTO WORD (value, IS_ARTICLE,IS_OBJ) VALUES ('SIT',FALSE ,FALSE);


--NOUN table

    --Word: ID = 1
    INSERT into NOUN (IS_PN,IS_SINGULAR,PLURAL_FORM,WORD_ID) values (true,true,'S',1);

    --Word: ID = 2
    INSERT into NOUN (IS_PN,IS_SINGULAR,PLURAL_FORM,WORD_ID) values (false,true,'S',2);

--VERB table

    --Word: ID = 3
    INSERT into VERB (PLURAL_FORM,WORD_ID) values ('S',3);