package org.oso.sengen.util;

import org.atteo.evo.inflector.English;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PluralTest {

    @Test
    public void testPluralForms() {
        assertEquals("SKIES", English.plural("SKY").toUpperCase());
        assertEquals("FOXES", English.plural("FOX").toUpperCase());
        assertEquals("MICE", English.plural("MOUSE").toUpperCase());
        assertEquals("FLIES", English.plural("FLY").toUpperCase());
        assertEquals("DOGS", English.plural("DOG").toUpperCase());
    }
}
