package org.oso.sengen.repository;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Verb;
import org.oso.sengen.model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class VerbRepositoryTest {

    @Autowired
    private VerbRepository verbRepository;

    @Autowired
    private WordRepository wordRepository;

    @Test
    public void verbRepositoryIsNotNull() {
        assertNotEquals(null, verbRepository);
    }

    @Test
    public void verbTableHasInitialDataLoad() {
        assertNotEquals(0, verbRepository.count());
    }


//	@Test
//	public void testVerbData() {
//		verbRepository.findVerbsByWord_Verb(true).forEach(verb -> {
//			assertEquals(true, verb.getWord().isVerb());
//			System.out.println(
//					verb.getId() + "\t" +
//							verb.getWord().getValue() + "\t" +
//							verb.getPf());
//		});
//	}

    @Test
    public void testFindByWord() {
        Optional<Verb> verb = verbRepository.findByWord(wordRepository.findById(3L).get());
        assertNotEquals(null, verb);
        verb.ifPresent(verb1 -> {
            log.info(verb1.getWord().getValue() + "\t" + verb1.getId());
        });
    }

    @Test
    public void testFindByWordValue() {
        Optional<Verb> verb = verbRepository.findByWord_Value("SIT");
        assertNotEquals(null, verb);
        verb.ifPresent(verb1 -> {
            log.info(verb1.getWord().getValue() + "\t" + verb1.getId());
        });
    }
}
