package org.oso.sengen.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class NounRepositoryTest {

	@Autowired
	private NounRepository nounRepository;

	@Test
	public void nounRepositoryIsNotNull() {
		assertNotEquals(null, nounRepository);
		log.info("Noun Repository is not null");
	}

	@Test
	public void nounTableHasInitialDataLoad() {
		assertNotEquals(0, nounRepository.count());
		log.info("Noun table is pre-populated");
	}

	@Test
	public void testNounData() {
		nounRepository.findAll().forEach(noun -> {
			assertNotEquals(null, noun.getWord());
			log.info(
					noun.getId() + "\t" +
							noun.getWord().getValue() + "\t" +
							noun.getPf());
		});
	}

	@Test
	public void testGetProperNounsOnly() {
		nounRepository.findNounsByPnEquals(true).forEach(
				noun -> {
					assertEquals(true, noun.isPn());
					log.info(noun.getWord().getValue());
				});
	}

	@Test
	public void testFindByWordValue() {
		Noun noun = nounRepository.findNounByWord_Value("TICKET");
		assertNotEquals(null, noun);
		log.info("Noun: " + noun.getWord().getValue() + " is found!");
	}

	@Test
	public void testWordAndNounRelationship() {
		Word word = new Word();
		word.setValue("BUS");

		Noun noun = new Noun();
		noun.setWord(word);

		log.info("Saving noun: " + noun.getWord().getValue());
		nounRepository.save(noun);

		Noun nounFromDB = nounRepository.findNounByWord_Value("BUS");
		assertNotEquals(null, nounFromDB.getWord().getValue());
		log.info("Found noun: " + nounFromDB.getWord().getValue());
	}
}
