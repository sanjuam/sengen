package org.oso.sengen.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
public class WordRepositoryTest {

	@Autowired
	private WordRepository wordRepository;

	@Autowired
	private NounRepository nounRepository;

	@Test
	public void wordRepositoryIsNotNull() {
		assertNotEquals(null, wordRepository);
		log.info("Word Repository is not null..");
	}

	@Test
	public void wordTableDataLoad() {
		assertNotEquals(0, wordRepository.count());
		log.info("Words are loaded properly from initial data load");
	}

	@Test
	public void findProperNounsOnly() {
		wordRepository.findWordsByNoun_Pn(true)
				.forEach(word -> {
					assertEquals(true,word.getNoun().isPn());
					log.info(word.getValue() + "\t");
				});
	}

	@Test
	public void findVerbWordsOnly() {
		wordRepository.findWordsByVerbIsNotNull()
				.forEach(word -> {
					assertNotNull(word.getVerb());
					log.info(word.getValue() + "\t");
				});
	}

	@Test
	public void findWordsWhereNounIsNotNull() {
		wordRepository.findWordsByNounIsNotNull()
				.forEach(word -> {
					assertNotEquals(null,word.getNoun());
					log.info(word.getValue() + "\t");
				});
	}

	@Test
	public void findWordThatisPlural() {
		wordRepository.findWordsByNounIsNotNull()
				.forEach(word -> {
					if (!word.getNoun().isSi()) {
						assertEquals(false, word.getNoun().isSi());
						log.info(word.getValue());
					}
				});
	}

	@Test
	public void findWordByValue() {
		Word word = wordRepository.findByValueEquals("SKY");
		log.info(word.getValue() + " : " + word.getNoun());
	}

	@Test
	public void testWordToNounRelationship() {
		Word word = new Word();
		word.setValue("JACKET");

		Noun noun = new Noun();
		noun.setWord(word);

		word.setNoun(noun);

		log.info("Inserting " + word.getValue() + " into db");
		wordRepository.save(word);

		Word wordFromDB = wordRepository.findByValueEquals("JACKET");
		assertNotEquals(null, wordFromDB.getNoun());
		log.info("Id of noun: " + wordFromDB.getNoun().getId());
	}
}
