package org.oso.sengen.service;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.WordRepository;
import org.oso.sengen.service.evoinflector.Pluralizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class PluralizerTest {

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private Pluralizer pluralizer;

    @Test
    public void wordRepositoryIsNotNull() {
        assertNotEquals(null, wordRepository);
        log.info("Word Repository is not null..");
    }

    @Test
    public void pluralizerIsNotNull() {
        assertNotEquals(null, pluralizer);
        log.info("Pluralizer service is not null...");
    }

    @Test
    public void pluralFormsAreSavedAndSet() {
        List<Word> words = new ArrayList<>();
        wordRepository.findAll().forEach(words::add);
        pluralizer.updatePluralForm(words);
        wordRepository.findAll().forEach(word -> {
            assertNotEquals(null, word.getPlural());
        });
        log.info("Plural forms are successfully updated!");
    }
}
