package org.oso.sengen.service.generator;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Sentence;
import org.oso.sengen.repository.WordRepository;
import org.oso.sengen.service.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertNotEquals;


@SpringBootTest
@Slf4j
public class GeneratorTest {

	@Autowired
	private WordRepository wordRepository;

	@Autowired
	private Random secureRandom;

	@Autowired
	private Generator generator;

	@Test
	public void secureRandomIsNotNull() {
		assertNotEquals(null, secureRandom);
	}

	@Test
	public void wordRepositoryIsNotNull() {
		assertNotEquals(null, wordRepository);
	}

	@Test
	public void normalSentenceGeneration() {
		List<Sentence> sentences = generator.generate();
		sentences.forEach(sentence -> log.info(sentence.getValue().toString() + "\t"));

	}
}
