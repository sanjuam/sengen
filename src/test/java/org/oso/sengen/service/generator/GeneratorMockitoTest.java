package org.oso.sengen.service.generator;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Sentence;
import org.oso.sengen.model.Verb;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.WordRepository;
import org.oso.sengen.service.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GeneratorMockitoTest {

	@InjectMocks
	private Generator generator;

	@Mock
	private WordRepository repository;

	@Mock
	private Random random;

	private static final String SPACE = " ";

	@Test
	void testSimpleGenerate() {
		List<Word> subjects = new ArrayList<>();
		List<Word> verbs = new ArrayList<>();
		List<Word> dirObjs = new ArrayList<>();

		Word subject = new Word();
		subject.setValue("MOM");
		Noun noun = new Noun();
		noun.setPn(true);

		subject.setNoun(noun);
		subjects.add(subject);

		Word verb = new Word();
		verb.setValue("HAS");
		verbs.add(verb);

		Word dirObj = new Word();
		Noun noun2 = new Noun();

		dirObj.setNoun(noun2);
		dirObj.setValue("JACKET");
		dirObjs.add(dirObj);

		when(repository.findWordsByVerbIsNotNull()).thenReturn(verbs);
		when(repository.findWordsByNounIsNotNull())
				.thenReturn(subjects)
				.thenReturn(dirObjs);
		when(random.nextInt(subjects.size())).thenReturn(0);
		when(random.nextInt(verbs.size())).thenReturn(0);
		when(random.nextInt(dirObjs.size())).thenReturn(0);

		//Test
		List<Sentence> sentences = generator.generate();
		sentences.forEach(sentence -> System.out.println(sentence.getValue()));
		assertEquals(5,sentences.size());
		assertEquals("MOM HAS THE JACKET ",
				sentences.get(1).getValue().toString());
	}

	@Test
	void testGenerateSubjectIsNotSameWithDO() {
		List<Word> subjects = new ArrayList<>();
		List<Word> verbs = new ArrayList<>();
		List<Word> dirObjs = new ArrayList<>();

		//Create Mock Subject
		Word subject = new Word();
		subject.setValue("MOM");

		Noun noun = new Noun();
		noun.setPn(true);

		subject.setNoun(noun);
		subjects.add(subject);

		//Create Mock Verb
		Word verb = new Word();
		verb.setValue("HAS");
		verbs.add(verb);


		//Create Mock Direct Objects
		Word dirObj = new Word();
		dirObj.setValue("MOM");
		Noun noun2 = new Noun();
		noun.setPn(true);

		dirObj.setNoun(noun2);
		dirObjs.add(dirObj);

		Word dirObj2 = new Word();
		dirObj2.setValue("JACKET");

		Noun noun3 = new Noun();

		dirObj2.setNoun(noun3);
		dirObjs.add(dirObj2);

		when(repository.findWordsByNounIsNotNull())
				.thenReturn(subjects)
				.thenReturn(dirObjs);
		when(repository.findWordsByVerbIsNotNull()).thenReturn(verbs);
		when(random.nextInt(subjects.size())).thenReturn(0);
		when(random.nextInt(verbs.size())).thenReturn(0);
		when(random.nextInt(dirObjs.size()))
				.thenReturn(0)
				.thenReturn(1);
		//Test
		System.out.println("Testing with Mockito");
		List<Sentence> sentences = generator.generate();
		sentences.forEach(sentence -> System.out.println(sentence.getValue()));
		assertNotEquals("MOM HAS MOM ",
				sentences.get(1).getValue().toString());
	}

	@Test
	public void singularNounVerbAgreement() {
		List<Word> subjects = new ArrayList<>();
		List<Word> verbs = new ArrayList<>();
		List<Word> dirObjs = new ArrayList<>();

		Word subject = new Word();
		subject.setValue("MOM");
		Noun noun = new Noun();
		noun.setPn(true);
		noun.setSi(true);

		subject.setNoun(noun);
		subjects.add(subject);

		Word verb = new Word();
		verb.setValue("KISS");

		Verb verb1 = new Verb();
		verb1.setPf("ES");
		verb.setVerb(verb1);
		verbs.add(verb);

		Word dirObj = new Word();
		Noun noun2 = new Noun();

		dirObj.setNoun(noun2);
		dirObj.setValue("JACKET");
		dirObjs.add(dirObj);

		when(repository.findWordsByNounIsNotNull())
				.thenReturn(subjects)
				.thenReturn(dirObjs);
		when(repository.findWordsByVerbIsNotNull()).thenReturn(verbs);
		when(random.nextInt(subjects.size())).thenReturn(0);
		when(random.nextInt(verbs.size())).thenReturn(0);
		when(random.nextInt(dirObjs.size())).thenReturn(0);

		//Test
		List<Sentence> sentences = generator.generate();
		sentences.forEach(sentence -> System.out.println(sentence.getValue()));
		assertEquals(5,sentences.size());
		assertEquals("MOM KISSES THE JACKET ",
				sentences.get(1).getValue().toString());

	}

	@Test
	public void pluralNounVerbAgreement() {
		List<Word> subjects = new ArrayList<>();
		List<Word> verbs = new ArrayList<>();
		List<Word> dirObjs = new ArrayList<>();

		Word subject = new Word();
		subject.setValue("RATS");
		Noun noun = new Noun();
		noun.setPn(true);
		noun.setSi(false);

		subject.setNoun(noun);
		subjects.add(subject);

		Word verb = new Word();
		verb.setValue("KISS");

		Verb verb1 = new Verb();
		verb1.setPf("ES");
		verb.setVerb(verb1);
		verbs.add(verb);

		Word dirObj = new Word();
		Noun noun2 = new Noun();

		dirObj.setNoun(noun2);
		dirObj.setValue("JACKET");
		dirObjs.add(dirObj);

		when(repository.findWordsByVerbIsNotNull()).thenReturn(verbs);
		when(repository.findWordsByNounIsNotNull())
				.thenReturn(subjects)
				.thenReturn(dirObjs);
		when(random.nextInt(subjects.size())).thenReturn(0);
		when(random.nextInt(verbs.size())).thenReturn(0);
		when(random.nextInt(dirObjs.size())).thenReturn(0);

		//Test
		List<Sentence> sentences = generator.generate();
		sentences.forEach(sentence -> System.out.println(sentence.getValue()));
		assertEquals(5,sentences.size());
		assertEquals("RATS KISS THE JACKET ",
				sentences.get(1).getValue().toString());

	}
}
