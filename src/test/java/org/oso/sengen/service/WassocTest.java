package org.oso.sengen.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class WassocTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void jsonMapperNotNull() {
        assertNotEquals(null, objectMapper);
    }

}
