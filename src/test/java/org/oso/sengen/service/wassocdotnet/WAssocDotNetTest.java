package org.oso.sengen.service.wassocdotnet;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Noun;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.NounRepository;
import org.oso.sengen.service.WAssocDotNetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class WAssocDotNetTest {

    @Autowired
    private WAssocDotNetService assocDotNetService;

    @Autowired
    private NounRepository nounRepository;

    @Test
    public void setAssocDotNetServiceIsNotNull() {
        assertNotEquals(null, assocDotNetService);
        log.info("WAssocDotNetService is not Null");
    }

    @Test
    public void updateActionsOfANounTest() {
        log.info("Testing WAssocDotNetService...");

        log.info("Creating word: BUGS");
        Word word = new Word();
        word.setValue("BUGS");

        Noun noun = new Noun();
        noun.setWord(word);
        nounRepository.save(noun);

        assertNotEquals(null, nounRepository.findNounByWord_Value("BUGS"));
        log.info("BUGS successfully saved in database");

        log.info("Creating sample list of words...");
        List<Word> words = Arrays.asList(word);

        assocDotNetService.updateActionsOfANoun(words);
        Noun nounFromDB = nounRepository.findNounByWord_Value(word.getValue());
        assertNotEquals(null, nounFromDB);
        assertNotEquals(null, nounFromDB.getVerbs());
    }

}
