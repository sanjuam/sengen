package org.oso.sengen.service.wordsapi;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oso.sengen.repository.WordRepository;

import static org.junit.jupiter.api.Assertions.assertNotEquals;


@Slf4j
public class WordsAPISvcMockitoTest {

//    @Mock
    private WordRepository wordRepository;

//    @InjectMocks
    private WordsAPISvc wordsAPISvc;

//    @Test
    void wordsAPISvcIsNotNull() {
        assertNotEquals(null, wordsAPISvc);
        log.info("Words API service is not null");
        assertNotEquals(null, wordRepository);
        log.info("Word Repository is not null");
    }
}
