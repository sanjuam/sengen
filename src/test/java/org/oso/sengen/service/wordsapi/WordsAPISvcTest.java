package org.oso.sengen.service.wordsapi;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.oso.sengen.model.Word;
import org.oso.sengen.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class WordsAPISvcTest {

    @Autowired
    private WordsAPISvc wordsAPISvc;

    @Autowired
    private WordRepository wordRepository;

    @Test
    public void wordsAPISvcIsNotNull() {
        assertNotEquals(null,wordsAPISvc);
        log.info("Words API service is not null");
    }

    @Test
    public void testassociateWord2NounsAndVerbs() {
        Word word = new Word();
        word.setValue("TRAP");
        List<Word> words = Arrays.asList(word);

        wordsAPISvc.associateWord2NounsAndVerbs(words);

        Word testWord_2 = wordRepository.findByValueEquals("TRAP");
        assertNotEquals(null, testWord_2.getNoun());

    }
}
