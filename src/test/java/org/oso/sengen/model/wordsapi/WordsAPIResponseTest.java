package org.oso.sengen.model.wordsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class WordsAPIResponseTest {

    @Autowired
    private ObjectMapper objectMapper;

    private File readJSONPayload() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("words-api.json").getFile());
        return file;
    }

    @Test
    public void objectMapperIsNotNull() {
        assertNotEquals(null, objectMapper);
        log.info("Object Mapper is not null");
    }

    @Test
    public void responseObjectIsCorrect() {
        try {
            WordsAPIResponse response = objectMapper.readValue(
                    readJSONPayload(),
                    WordsAPIResponse.class
            );
            assertNotEquals(null, response);
            assertNotEquals(null, response.getWord());
            assertNotEquals(null, response.getResults());
            log.info(response.getWord());
            response.getResults().forEach(
                    result -> {
                        log.info(result.getPartOfSpeech() + "\t" + result.getDefinition());
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
