package org.oso.sengen.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SentenceTest {

	@Test
	void testBuildSubjectWhenPN() {
		Word properNoun = new Word();
		properNoun.setValue("MOM");

		Noun noun = new Noun();
		noun.setPn(true);
		properNoun.setNoun(noun);

		Sentence sentence = new Sentence();
		String subject = sentence.buildSubject(properNoun);
		assertEquals("MOM ",subject);
	}

	@Test
	public void testPluralSubject() {
		Word subject = new Word();
		subject.setValue("BUGS");
		Noun noun = new Noun();
		noun.setSi(false);
		noun.setPn(false);
		subject.setNoun(noun);

		Word verb = new Word();
		verb.setValue("JUMP");
		Verb verb1 = new Verb();
		verb1.setPf("S");
		verb.setVerb(verb1);

		Word dirObj = new Word();
		dirObj.setValue("OFF THE WINDOW");
		Noun noun1 = new Noun();
		noun1.setPn(true);
		dirObj.setNoun(noun1);

		Sentence sentence = new Sentence(subject, verb, dirObj);
//		System.out.println(sentence.getValue().toString());
		assertEquals("THE BUGS JUMP OFF THE WINDOW ", sentence.getValue().toString());
	}
}
