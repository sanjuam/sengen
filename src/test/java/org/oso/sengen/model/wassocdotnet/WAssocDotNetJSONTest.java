package org.oso.sengen.model.wassocdotnet;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@Slf4j
public class WAssocDotNetJSONTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void objectMapperIsNotNull() {
        assertNotEquals(null, objectMapper);
    }


    @Test
    public void responseObjectIsCorrect() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("wassoc-dotnet.json").getFile());

        try {

            WAssocDotNetResponse response =  objectMapper.readValue(
                    file,
                    WAssocDotNetResponse.class);

            assertNotEquals(null, response);
            assertNotEquals(null, response.getRequest());
            assertNotEquals(null, response.getResponse());
            assertNotEquals(null, response.getVersion());

            response.getResponse().forEach(response1 -> {
                response1.getItems().forEach(item -> {
                    log.info(item.getItem() + "\t" + item.getPos());
                    assertEquals("verb",item.getPos());
                });
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
